# -*- coding: utf-8 -*-

import nltk
import os,errno
from nltk.tag import brill
from nltk.corpus import indian
import json
import sys

reload(sys)  
sys.setdefaultencoding('utf8')
print sys.getdefaultencoding()

dic={}

def splitArray(arr): # splite the data tranning data set into word VS tag
    try:
		return map(lambda token: (token.split('\\')[0],token.split('\\')[1].split(".")[0]), arr)
    except:
		pass

# create lexicon for primary knowledge
def wordLexicon(array):
    words={}
    tags={}
    sentence={}
    first_words={}
    max=0
   
    for i in array:
        aa=''
        b=[]
        x='o'
        tem_sen={}
        y='p'
        for j in i:
            a= j[0].encode('utf-8')
            #print type(a)
            if a in words: #to mapping word vs tags
                if j[1] in words[a]:
                    words[a][j[1]]=words[a][j[1]]+1
                else:
                    words[a].update({j[1]:1})
            else:
                words.update({a:{j[1]:1}})
            
            if aa!='':   #to mapping tag vs tags
                if j[1] in tags:
                    if aa in tags[j[1]]:
                        tags[j[1]][aa]=tags[j[1]][aa]+1
                    else:
                        tags[j[1]].update({aa:1})
                else:
                    tags.update({j[1]:{aa:1}})
            aa=j[1]
            b.append(j[1])
            if x=='o':  #to mapping sentence pattarn and create tree
                if j[1] in sentence:
                    tem_sen=sentence[j[1]]
                    
                else :
                    sentence.update({j[1]:{}})
                    tem_sen= sentence[j[1]]

            elif j[1]==x:
                pass
            else:
                if j[1] in tem_sen:
                    tem_sen=tem_sen[j[1]]
                else :
                    tem_sen.update({j[1]:{}})
                    tem_sen= tem_sen[j[1]]
                        #first words attribute
                if x in first_words and y=='p':
                    if j[1] in first_words[x]:
                        pass
                    else:
                        first_words[x].update({j[1]:{}})
                    y=x
                elif y=='p':
                    first_words.update({x:{j[1]:{}}})
                    y=x
                elif y!='p' and y!='q':
                    if j[1] in first_words[y][x]:
                        first_words[y][x][j[1]]=first_words[y][x][j[1]]+1
                    else:
                        first_words[y][x].update({j[1]:1})
                    y='q'
                        
            x=j[1]
    '''for i in first_words:
        for j in first_words[i]:
            print i,'>',j,'>',first_words[i][j]
            '''
                 

    # store words vs tag as jeson format
    for i in words:
        print type(i), i
    with open('Knowledge/words','w') as f:
        json.dump(words, f)
    with open('Knowledge/tags','w') as f:
        json.dump(tags, f)
    with open('Knowledge/sentence','w') as f:
        json.dump(sentence, f)
    with open('Knowledge/first_words','w') as f:
        json.dump(first_words, f)

# create file to save number of sentence and words
def stats(arr,dataset): 
    numLines=len(arr)
    numWords=len(reduce(lambda x,y:x+y,arr))
    lines=["Stats for dataset "+dataset+"\n","Number of lines: "+str(numLines)+"\n","Number of words: "+str(numWords)+"\n"]
    fwrite=open("analyzedData/"+dataset+".stat",'w')
    fwrite.writelines(lines)
    fwrite.close()

#to calculate frequency of sepcific tag
def posFrequency(arr,dataset): 
    arr=reduce(lambda x,y:x+y,arr)
    new_arr=map(lambda a:a[1],arr)
    lines=[]
    for e in set(new_arr):
        lines.append((e,new_arr.count(e)))
    lines=sorted(lines,key=lambda a:a[1],reverse=True)
    lines=map(lambda (a,b): str(a)+";"+str(b)+"\n", lines)
    fwrite=open("analyzedData/"+dataset+".freq",'w')
    fwrite.writelines(lines)
    fwrite.close()


fread=open("nltk_data/nltr")
lines=fread.readlines()
fread.close()

filtered_sents2=[]
lines=map(lambda line:line.split(),lines)
lines=map(lambda line:splitArray(line), lines)

for i,line in enumerate(lines):  #this is not necessary for learning
   
    try:
        tagger=nltk.tag.UnigramTagger([line])
        filtered_sents2.append(line)
        #print line
    except (TypeError, ValueError):
        pass

try:
	os.makedirs("analyzedData/")
except OSError as exc:
	if exc.errno == errno.EEXIST:
		pass
	else:
		raise
wordLexicon(filtered_sents2)
#save learning data information in analyzedData folder
'''stats(filtered_sents2,"nltr")  
posFrequency(filtered_sents2,"nltr")'''
